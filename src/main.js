import Vue from 'vue'
import App from './App.vue'
import apollo from './apolloClient'

Vue.config.productionTip = false
Vue.prototype.$apollo = apollo

new Vue({
    render: h => h(App),
}).$mount('#app')
